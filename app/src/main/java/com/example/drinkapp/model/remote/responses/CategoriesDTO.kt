package com.example.drinkapp.model.remote.responses

import com.google.gson.annotations.SerializedName

data class CategoriesDTO (
    @SerializedName("drinks")
    val categoryDTOS: List<CategoryDTO>
)