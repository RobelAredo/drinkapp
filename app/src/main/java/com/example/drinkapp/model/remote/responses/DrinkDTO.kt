package com.example.drinkapp.model.remote.responses


data class DrinkDTO(
    val idDrink: Int,
    val strDrink: String,
    val strDrinkThumb: String
)