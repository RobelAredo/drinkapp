package com.example.drinkapp.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.drinkapp.model.local.entity.Category
import com.example.drinkapp.model.local.entity.Drink

@Dao
interface DrinkDao {
    @Query("SELECT * FROM category")
    suspend fun getCategories(): List<Category>

    @Query("SELECT * FROM drink")
    suspend fun getAllDrinks(): List<Drink>

    @Query("SELECT * FROM drink WHERE strCategory=:category")
    suspend fun getAllDrinksFromCategory(category: String): List<Drink>

    @Query("SELECT * FROM drink WHERE idDrink=:id")
    suspend fun getDrinkById(id: Int): List<Drink>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(vararg category: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDrink(vararg drink: Drink)
}