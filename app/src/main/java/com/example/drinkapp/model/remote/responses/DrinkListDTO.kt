package com.example.drinkapp.model.remote.responses

data class DrinkListDTO(
    val drinks: List<DrinkDTO>
)