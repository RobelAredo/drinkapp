package com.example.drinkapp.model.remote

import com.example.drinkapp.model.remote.responses.CategoriesDTO
import com.example.drinkapp.model.remote.responses.DrinkDetailsDTO
import com.example.drinkapp.model.remote.responses.DrinkListDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinkService {
    companion object {
        private const val path  = "json/v1/1"
        private const val filter = "/filter.php"
    }

    @GET("$path/list.php")
    suspend fun getCategories(@Query("c") list: String = "list"): CategoriesDTO

    @GET("$path$filter")
    suspend fun getDrinksFromCategory(@Query("c") category: String): DrinkListDTO

    @GET("$path/lookup.php")
    suspend fun getDrinkDetailsById(@Query("i") drinkId: Int): DrinkDetailsDTO

    @GET("$path$filter")
    suspend fun getDrinksByAlcoholicContent(@Query("a") alcholic: String = "Alcoholic"): DrinkListDTO
}