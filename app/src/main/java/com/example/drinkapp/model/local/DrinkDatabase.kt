package com.example.drinkapp.model.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.drinkapp.model.local.dao.DrinkDao
import com.example.drinkapp.model.local.entity.Category
import com.example.drinkapp.model.local.entity.Drink


@Database(entities = [Drink::class, Category::class], version = 1, exportSchema = false)
abstract class DrinkDatabase: RoomDatabase() {
    abstract fun getDrinkDao(): DrinkDao
}