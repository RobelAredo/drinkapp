package com.example.drinkapp.model.remote.responses

data class DetailsDTO (
    val idDrink: Int,
    val strDrink: String,
    val strCategory: String,
    val strInstructions: String,
    val strDrinkThumb: String,
    val strImageAttribution: String
)