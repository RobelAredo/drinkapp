package com.example.drinkapp.model

import android.util.Log
import com.example.drinkapp.model.local.DrinkDatabase
import com.example.drinkapp.model.local.entity.Category
import com.example.drinkapp.model.local.entity.Drink
import com.example.drinkapp.model.remote.DrinkService
import com.example.drinkapp.model.remote.responses.DrinkDTO
import kotlinx.coroutines.*
import org.koin.java.KoinJavaComponent.inject

class DrinkRepo() {
    private val service by inject<DrinkService>(DrinkService::class.java)
    private val database by inject<DrinkDatabase>(DrinkDatabase::class.java)
    private val drinkDao by lazy { database.getDrinkDao() }

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        drinkDao.getCategories().ifEmpty {
            val categories = service.getCategories().categoryDTOS.map { Category(it.strCategory) }
            drinkDao.insertCategory(*categories.toTypedArray())
            return@ifEmpty categories
        }
    }

    suspend fun getDrinksFromCategory(category: String) = withContext(Dispatchers.IO) {
        drinkDao.getAllDrinksFromCategory(category).ifEmpty {
            val drinks = service.getDrinksFromCategory(category).drinks.map { Drink(it.idDrink, category, it.strDrink, it.strDrinkThumb) }
            drinkDao.insertDrink(*drinks.toTypedArray())
            return@ifEmpty drinks
        }
    }

    suspend fun getDrinkDetailsById(drinkId: Int): Drink = withContext(Dispatchers.IO) {
        var drinks = drinkDao.getDrinkById(drinkId)
        if (drinks.isEmpty() || drinks[0].instructions == "") {
            drinks = service.getDrinkDetailsById(drinkId).drinks
                .map {
                    Drink(
                    idDrink = it.idDrink,
                    strCategory = it.strCategory,
                    strDrink = it.strDrink,
                    strDrinkThumb = it.strDrinkThumb,
                    instructions = it.strInstructions
                )
            }
            drinkDao.insertDrink(drinks[0])
        }
        return@withContext drinks[0]
    }

    suspend fun getAllDrinks() = withContext(Dispatchers.IO){
        val cachedDrinks = drinkDao.getAllDrinks()
        if (cachedDrinks.size > 160) cachedDrinks
        else {
            val drinks = service.getDrinksByAlcoholicContent().drinks.toMutableList()
                .plus(service.getDrinksByAlcoholicContent("Non_Alcoholic").drinks)
                .shuffled()
                .loadDrinksInChunks(10)
            drinkDao.insertDrink(*drinks.toTypedArray())
            drinks
        }
    }

    private suspend fun List<DrinkDTO>.loadDrinksInChunks(chunkSize: Int) = withContext(Dispatchers.IO){
        val drinks = this@loadDrinksInChunks
        val chunksToLoad = mutableListOf<Deferred<List<Drink>>>()
        for(startIndex in drinks.indices step chunkSize) {
            val endIndex = if (startIndex + chunkSize > drinks.size) drinks.size else startIndex + chunkSize
            chunksToLoad.add(
                async {
                    drinks.subList(startIndex, endIndex)
                        .map { getDrinkDetailsById(it.idDrink) }
                }
            )
        }
        chunksToLoad.awaitAll().flatten().toList()
    }
}