package com.example.drinkapp.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Drink(
    @PrimaryKey(autoGenerate = false)
    val idDrink: Int,
    val strCategory: String,
    val strDrink: String,
    val strDrinkThumb: String,
    val instructions: String = ""
)