package com.example.drinkapp.model.remote.responses

data class DrinkDetailsDTO (
    val drinks: List<DetailsDTO>,
)
