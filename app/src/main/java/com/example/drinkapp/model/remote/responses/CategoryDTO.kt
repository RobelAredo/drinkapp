package com.example.drinkapp.model.remote.responses

data class CategoryDTO(val strCategory: String)