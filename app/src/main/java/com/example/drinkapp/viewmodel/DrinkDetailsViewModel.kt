package com.example.drinkapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.drinkapp.domain.GetDrinkDetailsUseCase
import com.example.drinkapp.model.local.entity.Drink
import com.example.drinkapp.view.ContentState
import com.example.drinkapp.view.State
import org.koin.java.KoinJavaComponent.inject

class DrinkDetailsViewModel(
    savedStateHandle: SavedStateHandle
): ViewModel() {
    private val getDrinkDetailsUseCase by inject<GetDrinkDetailsUseCase>(GetDrinkDetailsUseCase::class.java)

    private val drinkId: Int = savedStateHandle["drink_id"]!!
    val drink: LiveData<ContentState<Drink>> = liveData {
        emit(ContentState())
        val result = getDrinkDetailsUseCase.invoke(drinkId)
        emit(
            ContentState(
                state = result.getState(),
                content = result.getOrNull(),
                errMsg = result.exceptionOrNull()?.message ?: ""
            )
        )
    }

    private fun Result<Drink>.getState() =
        if (isSuccess) State.SUCCESS else State.FAILURE
}