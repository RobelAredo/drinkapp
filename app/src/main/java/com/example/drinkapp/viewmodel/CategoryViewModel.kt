package com.example.drinkapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.drinkapp.domain.Category
import com.example.drinkapp.domain.GetAllDrinksUseCase
import com.example.drinkapp.domain.GetCategoriesUseCase
import com.example.drinkapp.model.local.entity.Drink
import com.example.drinkapp.view.ContentState
import com.example.drinkapp.view.State
import org.koin.java.KoinJavaComponent.inject


class CategoryViewModel() : ViewModel() {

    private val getCategoriesUseCase by inject<GetCategoriesUseCase>(GetCategoriesUseCase::class.java)
    private val getAllDrinksUseCase by inject<GetAllDrinksUseCase>(GetAllDrinksUseCase::class.java)

    val categories: LiveData<ContentState<List<Category>>> = liveData {
        emit(ContentState())
        val result = getCategoriesUseCase.invoke()
        emit(
            ContentState(
                state = result.getState(),
                content = result.getOrNull() ?: emptyList(),
                errMsg = result.exceptionOrNull()?.message ?: ""
            )
        )
    }

    val allDrinks: LiveData<ContentState<List<Drink>>> = liveData {
        emit(ContentState())
        val result = getAllDrinksUseCase.invoke()
        emit(
            ContentState(
                state = result.getState(),
                content = result.getOrNull(),
                errMsg = result.exceptionOrNull()?.message ?: ""
            )
        )
    }

    private fun <T> Result<T>.getState() =
        if (isSuccess) State.SUCCESS else State.FAILURE
}