package com.example.drinkapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.drinkapp.domain.GetDrinksUseCase
import com.example.drinkapp.model.local.entity.Drink
import com.example.drinkapp.view.ContentState
import com.example.drinkapp.view.State
import org.koin.java.KoinJavaComponent.inject

class DrinksViewModel(
    private val savedStateHandle: SavedStateHandle
): ViewModel() {
    private val getDrinksUseCase by inject<GetDrinksUseCase>(GetDrinksUseCase::class.java)

    private val category: String = savedStateHandle["category"]!!
    val drinks: LiveData<ContentState<List<Drink>>> = liveData {
        emit(ContentState())
        val result = getDrinksUseCase.invoke(category)
        emit(
            ContentState(
                state = result.getState(),
                content = result.getOrNull() ?: emptyList(),
                errMsg = result.exceptionOrNull()?.message ?: ""
            )
        )
    }

    private fun Result<List<Drink>>.getState() =
        if (isSuccess) State.SUCCESS else State.FAILURE
}