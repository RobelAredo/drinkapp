package com.example.drinkapp.view

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.drinkapp.databinding.FragmentDrinkDetailsBinding
import com.example.drinkapp.model.local.entity.Drink
import com.example.drinkapp.viewmodel.DrinkDetailsViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class DrinkDetailsFragment: Fragment() {
    private var _binding: FragmentDrinkDetailsBinding? = null
    private val binding get() = _binding!!
    private val drinkDetailsViewModel by viewModels<DrinkDetailsViewModel>()
    private val args by navArgs<DrinkDetailsFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkDetailsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            title.text = args.drink
            drinkDetailsViewModel.drink.observe(viewLifecycleOwner) { state ->
                displayDrinkDetails(state)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun FragmentDrinkDetailsBinding.displayDrinkDetails(state: ContentState<Drink>) {
        piLoading.visibility = View.GONE
        when (state.state) {
            State.LOADING -> piLoading.visibility = View.VISIBLE
            State.FAILURE -> displayError(state.errMsg)
            State.SUCCESS -> displayDetails(state.content!!)
        }
    }


    private fun FragmentDrinkDetailsBinding.displayDetails(drink: Drink){
        ivDrink.load(drink.strDrinkThumb)
        tvDescription.text = "A great ${drink.strCategory} with friends!"
        tvInstructions.text = drink.instructions
    }

    private fun displayError(msg: String) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Could Not Find Categories")
            .setMessage(msg)
            .setNeutralButton("OK") { dialogInterface: DialogInterface, i: Int -> }
            .show()
    }
}