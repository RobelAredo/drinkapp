package com.example.drinkapp.view

data class ContentState <T> (
    val state: State = State.LOADING,
    val content: T? = null,
    val errMsg: String = ""
)

enum class State {
    LOADING,
    FAILURE,
    SUCCESS
}