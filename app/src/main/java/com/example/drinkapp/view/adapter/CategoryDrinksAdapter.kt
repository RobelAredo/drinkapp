package com.example.drinkapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavDirections
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.drinkapp.databinding.ItemDrinkBinding
import com.example.drinkapp.model.local.entity.Drink
import com.example.drinkapp.view.DrinksFragmentDirections

class CategoryDrinksAdapter (
    private val nav: (NavDirections) -> Unit
): RecyclerView.Adapter<CategoryDrinksAdapter.DrinksViewHolder>() {
    private var drinks = mutableListOf<Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DrinksViewHolder(ItemDrinkBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ), nav)

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        val drink = drinks[position]
        holder.addDrinkCategories(drink)
    }

    override fun getItemCount() = drinks.size

    fun addDrinks(drinksList: List<Drink>) {
        val oldSize = drinks.size
        drinks.clear()
        notifyItemRangeChanged(oldSize, 0)
        drinks = drinksList.toMutableList()
        notifyItemRangeChanged(0, drinks.size)
    }

    class DrinksViewHolder(
        private val binding: ItemDrinkBinding,
        private val nav: (NavDirections) -> Unit
    ): RecyclerView.ViewHolder(binding.root) {
        fun addDrinkCategories(drink: Drink) {
            binding.run {
                ivCategoryExample.load(drink.strDrinkThumb)
                tvDrinkName.text = drink.strDrink
                goToDrinkDetailsFromDrinks(tvDrinkName, ivCategoryExample, drink = drink)
            }
        }

        private fun goToDrinkDetailsFromDrinks(vararg views: View, drink: Drink) {
            val directions =  DrinksFragmentDirections.actionDrinksFragmentToDrinkDetailsFragment(drink.strDrink, drink.idDrink)
            for (view in views) view.navigate(directions)
        }

        private fun View.navigate(directions: NavDirections) {
            setOnClickListener{ nav(directions) }
        }

    }
}