package com.example.drinkapp.view

import android.content.*
import android.media.MediaPlayer
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.drinkapp.R
import com.example.drinkapp.databinding.FragmentCategoryBinding
import com.example.drinkapp.domain.Category
import com.example.drinkapp.model.local.entity.Drink
import com.example.drinkapp.services.MusicService
import com.example.drinkapp.view.adapter.CategoryAdapter
import com.example.drinkapp.view.adapter.DrinksAdapter
import com.example.drinkapp.viewmodel.CategoryViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class CategoryFragment: Fragment() {
    private var _binding: FragmentCategoryBinding? = null
    private val binding: FragmentCategoryBinding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()

    private val categoryAdapter by lazy { CategoryAdapter(::nav) }
    private val drinksAdapter by lazy { DrinksAdapter(::nav) }
    private var service: MusicService? = null
    private var bound = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(layoutInflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            tvTitle.text
            rvCategories.adapter = categoryAdapter
            rvAllDrinks.adapter = drinksAdapter

            play.setOnClickListener {
                Intent(context, MusicService::class.java).also {
                    activity?.startService(it)
                }
            }

            playBound.setOnClickListener {
                Intent(context, MusicService::class.java).also {
                    activity?.bindService(it, serviceConnection, Context.BIND_AUTO_CREATE)
                }
//                service?.playMusic()
            }

            stop.setOnClickListener {
                Intent(context, MusicService::class.java).also {
                    activity?.stopService(it)
                }
            }

            stopBound.setOnClickListener {
//                service?.stopMusic()
                if(bound) activity?.unbindService(serviceConnection)
                bound = false
            }

            secondActivityBtn.setOnClickListener {
                findNavController().navigate(CategoryFragmentDirections.actionCategoryFragmentToSecondActivity())
            }

            categoryViewModel.categories.observe(viewLifecycleOwner){ state ->
                displayCategories(state)
            }

            categoryViewModel.allDrinks.observe(viewLifecycleOwner){ state ->
                displayDrinks(state)
            }
        }
    }

    override fun onDestroy() {
//        Intent(context, MusicService::class.java).also {
//            activity?.stopService(it)
//        }
        _binding = null
        super.onDestroy()
    }

    val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            service = (p1 as MusicService.MusicBinder).musicService
            bound = true
            Log.d("CategoryFragment", "OnServiceConnected")
            MediaPlayer.create(activity, R.raw.ekout_good_morning).start()
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            service = null
            Log.d("CategoryFragment", "OnServiceDisconnected")
        }
    }

    private fun FragmentCategoryBinding.displayCategories(state: ContentState<List<Category>>) {
        piLoading.visibility = View.GONE
        when(state.state) {
            State.LOADING -> piLoading.visibility =  View.VISIBLE
            State.FAILURE -> displayError(state.errMsg, "Categories")
            State.SUCCESS -> categoryAdapter.addCategories(state.content!!)
        }
    }

    private fun FragmentCategoryBinding.displayDrinks(state: ContentState<List<Drink>>) {
        piLoading.visibility = View.GONE
        when(state.state) {
            State.LOADING -> piLoading.visibility =  View.VISIBLE
            State.FAILURE -> displayError(state.errMsg, "Drinks")
            State.SUCCESS -> drinksAdapter.addDrinks(state.content!!)
        }
    }

    private fun displayError(msg: String, title: String) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Could Not Find $title")
            .setMessage(msg)
            .setNegativeButton("OK") { dialogInterface: DialogInterface, i: Int -> }
            .show()
    }

    private fun nav(directions: NavDirections) {
        findNavController().navigate(directions)
    }
}