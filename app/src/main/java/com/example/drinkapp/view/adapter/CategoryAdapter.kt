package com.example.drinkapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavDirections
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.drinkapp.databinding.ItemCategoryBinding
import com.example.drinkapp.domain.Category
import com.example.drinkapp.view.CategoryFragmentDirections

class CategoryAdapter (
    private val nav: (NavDirections) -> Unit
): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    private var drinkCategories = mutableListOf<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoryViewHolder(ItemCategoryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ), nav)

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = drinkCategories[position]
        holder.addDrinkCategories(category)
    }

    override fun getItemCount() = drinkCategories.size

    fun addCategories(categories: List<Category>) {
        val oldSize = drinkCategories.size
        drinkCategories.clear()
        notifyItemRangeChanged(oldSize, 0)
        drinkCategories = categories.toMutableList()
        notifyItemRangeChanged(0, drinkCategories.size)
    }

    class CategoryViewHolder(
        private val binding: ItemCategoryBinding,
        private val nav: (NavDirections) -> Unit
    ): RecyclerView.ViewHolder(binding.root) {
        fun addDrinkCategories(category: Category) {
            binding.run {
                ivCategoryExample.load(category.exampleDrink.strDrinkThumb)
                tvCategory.text = category.name
                tvExampleTag.text = category.exampleDrink.strDrink
                goToDrinksFromCategory(tvCategory, ivCategoryExample, category = category.name)
            }
        }

        private fun formatTag(tag: String) =
            if (tag.length > 13) "${tag.substring(0,10)}..." else tag


        private fun goToDrinksFromCategory(vararg views: View, category: String) {
            val directions =  CategoryFragmentDirections.actionCategoryFragmentToDrinksFragment(category)
            for (view in views) view.navigate(directions)
        }

        private fun goToDrinkDetails(vararg views: View, drink: String) {
//            val directions =  CategoryFragmentDirections.actionCategoryFragmentToDrinksFragment()
//            for (view in views) view.nav(directions)
        }

        private fun View.navigate(directions: NavDirections) {
            setOnClickListener{ nav(directions) }
        }

    }
}