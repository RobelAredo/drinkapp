package com.example.drinkapp.view

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.drinkapp.databinding.FragmentDrinksBinding
import com.example.drinkapp.model.local.entity.Drink
import com.example.drinkapp.view.adapter.CategoryDrinksAdapter
import com.example.drinkapp.viewmodel.DrinksViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class DrinksFragment: Fragment() {
    private var _binding: FragmentDrinksBinding? = null
    private val binding get() = _binding!!
    private val drinksViewModel by viewModels<DrinksViewModel>()
    private val drinksAdapter by lazy { CategoryDrinksAdapter(::nav) }
    private val args by navArgs<DrinksFragmentArgs>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinksBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            title.text = args.category
            rvCategories.adapter = drinksAdapter
            drinksViewModel.drinks.observe(viewLifecycleOwner) { state ->
                displayDrinks(state)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun FragmentDrinksBinding.displayDrinks(state: ContentState<List<Drink>>) {
        piLoading.visibility = View.GONE
        when (state.state) {
            State.LOADING -> piLoading.visibility = View.VISIBLE
            State.FAILURE -> displayError(state.errMsg)
            State.SUCCESS -> drinksAdapter.addDrinks(state.content!!)
        }
    }

    private fun displayError(msg: String) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Could Not Find Categories")
            .setMessage(msg)
            .setNeutralButton("OK") { dialogInterface: DialogInterface, i: Int -> }
            .show()
    }

    private fun nav(directions: NavDirections) {
        findNavController().navigate(directions)
    }
}