package com.example.drinkapp.view

import android.app.Application
import com.example.drinkapp.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class DrinkApp: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidLogger()
            androidContext(this@DrinkApp)
            modules(
                remoteModule,
                dispatcherModule,
                useCaseModule,
                viewModelModule,
                repoModule,
                localModule
            )
        }
    }
}