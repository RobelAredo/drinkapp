package com.example.drinkapp.view

import android.app.DownloadManager
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.drinkapp.R
import com.example.drinkapp.broadcasts.AirplaneModeBroadcastReceiver
import com.example.drinkapp.services.MusicService

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    lateinit var airplaneModeReceiver: AirplaneModeBroadcastReceiver
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)


//        IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE).apply {
//            registerReceiver()
//        }
    }

    override fun onStart() {
        super.onStart()
        airplaneModeReceiver = AirplaneModeBroadcastReceiver()
        IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED).apply {
            registerReceiver(airplaneModeReceiver, this)
        }
    }

    override fun onDestroy() {
        unregisterReceiver(airplaneModeReceiver)
        super.onDestroy()
    }
}