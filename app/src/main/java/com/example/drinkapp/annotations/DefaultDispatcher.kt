package com.example.drinkapp.annotations

import javax.inject.Qualifier

@Target(AnnotationTarget.EXPRESSION, AnnotationTarget.PROPERTY)
@Qualifier
@Retention(AnnotationRetention.SOURCE)
annotation class DefaultDispatcher