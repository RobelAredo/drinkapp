package com.example.drinkapp.annotations

import javax.inject.Qualifier

@Target(AnnotationTarget.EXPRESSION)
@Qualifier
@Retention(AnnotationRetention.SOURCE)
annotation class IODispatcher