package com.example.drinkapp.services

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.example.drinkapp.R

class MusicService: Service() {
    private val binder = MusicBinder()
    companion object {
        private var _player: MediaPlayer? = null
        private val player: MediaPlayer get() = _player!!
    }
    override fun onBind(p0: Intent?): IBinder {
        playMusic()
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d("MusicService", "OnUnbind")
        stopMusic()
        return super.onUnbind(intent)
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        playMusic()
        return START_STICKY
    }

    override fun onDestroy() {
        stopMusic()
        super.onDestroy()
    }

    inner class MusicBinder: Binder() {
        val musicService: MusicService = this@MusicService
    }

    fun playMusic() {
        if (_player == null) _player = MediaPlayer.create(this, R.raw.ekout_good_morning)
        Log.d("MusicService playMusic", _player.toString())
        player.start()
    }

    fun stopMusic() {
        Log.d("MusicService stopMusic", _player.toString())
        if(_player != null) player.stop()
//        _player = null
    }
}