package com.example.drinkapp.di

import com.example.drinkapp.viewmodel.CategoryViewModel
import com.example.drinkapp.viewmodel.DrinkDetailsViewModel
import com.example.drinkapp.viewmodel.DrinksViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { CategoryViewModel() }
    viewModel { DrinksViewModel(get()) }
    viewModel { DrinkDetailsViewModel(get()) }
}