package com.example.drinkapp.di

import com.example.drinkapp.model.DrinkRepo
import org.koin.dsl.module


val repoModule = module {
    single { DrinkRepo() }
}