package com.example.drinkapp.di

import com.example.drinkapp.annotations.DefaultDispatcher
import com.example.drinkapp.annotations.IODispatcher
import com.example.drinkapp.annotations.MainDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val dispatcherModule = module {

    @DefaultDispatcher
    single { Dispatchers.Default }

//    @IODispatcher
//    single { Dispatchers.IO }
//
//    @MainDispatcher
//    single { Dispatchers.Main }


}