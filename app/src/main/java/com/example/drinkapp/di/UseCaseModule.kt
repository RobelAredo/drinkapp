package com.example.drinkapp.di

import com.example.drinkapp.domain.GetAllDrinksUseCase
import com.example.drinkapp.domain.GetCategoriesUseCase
import com.example.drinkapp.domain.GetDrinkDetailsUseCase
import com.example.drinkapp.domain.GetDrinksUseCase
import org.koin.dsl.module


val useCaseModule = module {
    single { GetAllDrinksUseCase() }
    single { GetCategoriesUseCase() }
    single { GetDrinkDetailsUseCase() }
    single { GetDrinksUseCase() }
}