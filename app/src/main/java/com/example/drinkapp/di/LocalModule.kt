package com.example.drinkapp.di

import androidx.room.Room
import com.example.drinkapp.model.local.DrinkDatabase
import org.koin.dsl.module


private const val DB_NAME = "drinks"
val localModule = module {
    single{
        Room.databaseBuilder(get(), DrinkDatabase::class.java, DB_NAME).build()
    }
}