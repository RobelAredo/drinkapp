package com.example.drinkapp.di

import com.example.drinkapp.model.remote.DrinkService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create


private const val BASE_URL = "https://www.thecocktaildb.com/api/"
val remoteModule = module {
    single<Gson> { GsonBuilder().setLenient().create() }

    single<DrinkService> {
        Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .build().create()
    }
}