package com.example.drinkapp.domain

import com.example.drinkapp.annotations.DefaultDispatcher
import com.example.drinkapp.model.DrinkRepo
import com.example.drinkapp.model.local.entity.Drink
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class GetDrinksUseCase () {
    private val repo by inject<DrinkRepo>(DrinkRepo::class.java)
    @DefaultDispatcher
    private val dispatcher by inject<CoroutineDispatcher>(CoroutineDispatcher::class.java)

    suspend operator fun invoke(category: String): Result<List<Drink>> = withContext(dispatcher) {
        try {
            val drinkList = repo.getDrinksFromCategory(category)
            Result.success(drinkList)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }

}