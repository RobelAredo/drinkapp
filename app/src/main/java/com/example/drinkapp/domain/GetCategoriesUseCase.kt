package com.example.drinkapp.domain

import com.example.drinkapp.annotations.DefaultDispatcher
import com.example.drinkapp.model.DrinkRepo
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class GetCategoriesUseCase() {
    private val repo by inject<DrinkRepo>(DrinkRepo::class.java)
    @DefaultDispatcher
    private val dispatcher by inject<CoroutineDispatcher>(CoroutineDispatcher::class.java)

    suspend operator fun invoke(): Result<List<Category>> = withContext(dispatcher) {
        try {
            val categoriesList = repo.getCategories()
            val categoriesWithExample = categoriesList.map {
                async {
                    repo.getDrinksFromCategory(it.strCategory).random().let {
                        Category(it.strCategory, it)
                    }
                }
            }.awaitAll()
            Result.success(categoriesWithExample)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }

}