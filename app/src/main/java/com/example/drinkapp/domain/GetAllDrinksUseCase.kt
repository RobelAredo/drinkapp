package com.example.drinkapp.domain

import android.util.Log
import com.example.drinkapp.annotations.DefaultDispatcher
import com.example.drinkapp.model.DrinkRepo
import com.example.drinkapp.model.local.entity.Drink
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class GetAllDrinksUseCase() {
    private val repo by inject<DrinkRepo>(DrinkRepo::class.java)
    @DefaultDispatcher private val dispatcher by inject<CoroutineDispatcher>(CoroutineDispatcher::class.java)

    suspend operator fun invoke(): Result<List<Drink>> = withContext(dispatcher) {
        try {
            val drinksList = repo.getAllDrinks()
            Result.success(drinksList)
        } catch (e: Exception) {
            e.printStackTrace()
            Result.failure(e)
        }
    }

}