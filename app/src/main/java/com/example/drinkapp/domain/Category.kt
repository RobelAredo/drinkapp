package com.example.drinkapp.domain

import com.example.drinkapp.model.local.entity.Drink

data class Category(
    val name: String,
    val exampleDrink: Drink
)