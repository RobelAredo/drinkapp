package com.example.drinkapp.domain

import com.example.drinkapp.annotations.DefaultDispatcher
import com.example.drinkapp.model.DrinkRepo
import com.example.drinkapp.model.local.entity.Drink
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class GetDrinkDetailsUseCase() {
    private val repo by inject<DrinkRepo>(DrinkRepo::class.java)
    @DefaultDispatcher
    private val dispatcher by inject<CoroutineDispatcher>(CoroutineDispatcher::class.java)

    suspend operator fun invoke(drinkId: Int): Result<Drink> = withContext(dispatcher) {
        try {
            val drinkDetails = repo.getDrinkDetailsById(drinkId)
            Result.success(drinkDetails)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }

}