package com.example.drinkapp.broadcasts

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast

class DownloadBroadcastReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val id: Long = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1) ?: return
//        Toast.makeText(context, notifyWifi(airplaneModeEnabled), Toast.LENGTH_SHORT).show()
    }

    private fun notifyWifi(on: Boolean): String {
        return "Airplane Mode is " + if(on)  "On" else "Off"
    }
}
