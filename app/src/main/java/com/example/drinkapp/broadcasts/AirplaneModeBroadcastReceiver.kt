package com.example.drinkapp.broadcasts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast

class AirplaneModeBroadcastReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("Receiver", "HERE")
        val airplaneModeEnabled = intent?.getBooleanExtra("state", false) ?: return
        Toast.makeText(context, notifyWifi(airplaneModeEnabled), Toast.LENGTH_SHORT).show()
    }

    private fun notifyWifi(on: Boolean): String {
        return "Airplane Mode is " + if(on)  "On" else "Off"
    }
}
